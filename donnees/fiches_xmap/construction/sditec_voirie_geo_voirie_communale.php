<?php
?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        #sw_fiche_edition {
            min-width: 550px !important;
			min-height: 250px !important;
        }
		
		.panel-body{
			 width:550px !important;
		}
		
		#sw_ficheEdt
		{
			min-width: 450px !important;
			min-height: 200px !important;
			max-height: auto !important;
		}
		
		#sw_drawing_attForm {
            min-width: 450px !important;
			min-height: 200px !important;
			overflow: auto;
        }
    </style>
</head> 

<div id="sw_drawing_attForm" class="tabbable">
    <h3 class="smaller blue no-margin-top">
                Classement Voie communale - Chemin rural
            </h3>
            <!--ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab-fiche-general" data-toggle="tab">Saisie voie</a>
                </li>
            </ul-->
   <form id="drawingFormPiv" class="form-horizontal">
      <?php
		//echo $this->getHiddenField($this->properties['gid']);
		$prop['insee'] = $this->layerProperties['insee'];

		if (is_array($prop['insee']['value']))
				{
				$colInsee = $prop['insee']['field']['intersects']['value'];
				$prop['insee']['value'] = $prop['insee']['value'][0][$colInsee];
				}

		?>
      <!-- Général -->	 
			<div class="tab-content">
                    <div class="tab-pane in active" id="tab-fiche-general">
						<div class="form-group">
							<div class="col-sm-3">
								%%insee_label%%
							</div>
							<div class="col-sm-9">
								%%insee_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%ident_label%%
							</div>
							<div class="col-sm-9">
								%%ident_value%%
							</div>
						</div>
						<div class="form-group">	
							<div class="col-sm-3">
								%%statut_voie_label%%
							</div>
							<div class="col-sm-9">
								%%statut_voie_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%caractere_voie_label%%
							</div>
							<div class="col-sm-9">
								%%caractere_voie_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%appellation_label%%
							</div>
							<div class="col-sm-9">
								%%appellation_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%p_origine_label%%
							</div>
							<div class="col-sm-9">
								%%p_origine_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%p_extremite_label%%
							</div>
							<div class="col-sm-9">
								%%p_extremite_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%lieudit_label%%
							</div>
							<div class="col-sm-9">
								%%lieudit_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%longtronc_saisie_label%%
							</div>
							<div class="col-sm-9">
								%%longtronc_saisie_value%%
							</div>
						</div>						
						<div class="form-group">
							<div class="col-sm-3">
								%%longueur_totale_label%%
							</div>
							<div class="col-sm-9">
								%%longueur_totale_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%largeur_emprise_moy_label%%
							</div>
							<div class="col-sm-9">
								%%largeur_emprise_moy_value%%
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								%%ancienne_voie_label%%
							</div>
							<div class="col-sm-9">
								%%ancienne_voie_value%%
							</div>
						</div>	
						<div class="form-group">
							<div class="col-sm-3">
								%%existence_dernier_classement_label%%
							</div>
							<div class="col-sm-9">
								%%existence_dernier_classement_value%%
							</div>
						</div>	
						<div class="form-group">
							<div class="col-sm-3">
								%%annee_classement_label%%
							</div>
							<div class="col-sm-9">
								%%annee_classement_value%%
							</div>
						</div>	
						<div class="form-group">
							<div class="col-sm-3">
								%%observations_label%%
							</div>
							<div class="col-sm-9">
								%%observations_value%%
							</div>
						</div>							
                    </div>
            </div>
   </form>
</div>
<script>
$(document).ready(function() {
  $('#sw_fiche_edition').on('afterShowAttPanel', function() {
  });
});
</script>