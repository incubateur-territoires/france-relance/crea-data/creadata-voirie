-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/03 : SL / Création du fichier sur Git
-- 2022/05/16 : SL / Ajout des champ [CCLTD]

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                              Table géographique : Programme d'entretien de la voirie sauvegarde initiale (cdc)                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.geo_programme_entretien_save_tmp_cdc;

CREATE TABLE sditec_voirie.geo_programme_entretien_save_tmp_cdc
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(50), --[SIRAP] Identifiant SIRAP / Nom du tronçon, de la voie, de la voirie ou du programme au choix du créateur
    numero_voie varchar(20), --[ATD16] Numéro de la voie
    nom_voie varchar(255), --[ATD16] Nom de la voie
    longtronc_sig real, --[ATD16] Longueur du tronçon (automatique)
    trafic_leger varchar(2), --[CCLTD] Appréciation du trafic léger sur la voie
    trafic_lourd varchar(2), --[CCLTD] Appréciation du trafic lourd sur la voie
    nature_ss_sol varchar(2), --[CCLTD] Nature du sous-sol
    secteur_boise varchar(2), --[CCLTD] Présence de secteur boisé
    etat_route varchar(2), --[CCLTD] État général actuel de la voie
    voie_transit varchar(2), --[CCLTD] Voie de transit (liaison entre voie importante, desserte village)
    nature_revetement varchar(255), --[CCLTD] Nature du revêtement
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_programme_entretien_save_tmp_cdc PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.geo_programme_entretien_save_tmp_cdc OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.geo_programme_entretien_save_tmp_cdc TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.geo_programme_entretien_save_tmp_cdc IS 
    '[ATD16] Table contenant la géométrie des tronçons de voiries faisant partie d''un programme d''entretien par la cdc. Sauvegarde de la table geo_programme_entretien_cdc pour avoir un état initial (demande de la ccltd). Table destinée à la consultation';

COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.ident IS 
    '[SIRAP] Identifiant SIRAP / Nom du tronçon, de la voie, de la voirie ou du programme au choix du créateur';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.numero_voie IS '[ATD16] Numéro de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.nom_voie IS '[ATD16] Nom de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.longtronc_sig IS '[ATD16] Longueur de l''objet (non automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.trafic_leger IS '[CCLTD] Appréciation du trafic léger sur la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.trafic_lourd IS '[CCLTD] Appréciation du trafic lourd sur la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.nature_ss_sol IS '[CCLTD] Nature du sous-sol';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.secteur_boise IS '[CCLTD] Présence de secteur boisé';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.etat_route IS '[CCLTD] État général actuel de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.voie_transit IS '[CCLTD] Voie de transit (liaison entre voie importante, desserte village)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.nature_revetement IS '[CCLTD] Nature du revêtement';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.date_creation IS '[ATD16] Date de création de l''objet (non automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (non automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_save_tmp_cdc.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

