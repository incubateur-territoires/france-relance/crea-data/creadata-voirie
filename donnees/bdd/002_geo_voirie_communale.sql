-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Cr�ation du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table g�ographique : Lin�aire de la voirie communale                                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.geo_voirie_communale;

CREATE TABLE sditec_voirie.geo_voirie_communale
(
  gid serial NOT NULL,
  insee character varying(6),				/* N� insee de la commune */
  ident character varying(40),				/* N� de la voie */
  statut_voie character varying(10),		
  caractere_voie character varying(10),
  appellation character varying(255),
  p_origine character varying(255),
  p_extremite character varying(255),
  lieudit varchar(256),
  longtronc_saisie real,					/* longueur du tron�on saisie par l'utilisateur */
  longtronc_sig real,  						/* longueur du tron�on calcul�e par une fonction automatique */
  longueur_totale real,						/* longueur totale de la voie saisie par l'utilisateur */
  largeur_emprise_moy real,  
  ancienne_voie character varying(255),
  existence_dernier_classement varchar(2),
  annee_classement character varying(10),
  observations character varying(500),
  url character varying(2048), 	
  origdata character varying(255),  
  date_creation timestamp,
  date_maj timestamp,
  longtronc_saisie real,
  the_geom geometry,
  CONSTRAINT geo_voirie_communale_pk PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sditec_voirie.geo_voirie_communale
  OWNER TO sditecgrp;


-- ################################################################## Commentaires ##################################################################



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Trigger(s)                                                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################


-- DROP TRIGGER sditec_voirie.t_before_i_date_creation ON sditec_voirie.geo_voirie_communale;

CREATE TRIGGER t_before_i_date_creation
	BEFORE INSERT
	ON sditec_voirie.geo_voirie_communale
	FOR EACH ROW
	EXECUTE PROCEDURE  sditec_voirie.f_date_creation();



-- ##################################################################################################################################################
-- ###                                                       Mise � jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER sditec_voirie.t_before_u_date_maj ON sditec_voirie.geo_voirie_communale ;

CREATE TRIGGER t_before_u_date_maj
	BEFORE UPDATE
	ON sditec_voirie.geo_voirie_communale
	FOR EACH ROW
	EXECUTE PROCEDURE  sditec_voirie.f_date_maj();

-- ##################################################################################################################################################
-- ###                                                       Mise � jour du champ longtronc_sig                                                   ###
-- ##################################################################################################################################################

-- DROP TRIGGER IF EXISTS sditec_voirie.t_before_iu_longtronc_sig ON sditec_voirie.geo_voirie_communale;

CREATE TRIGGER t_before_iu_longtronc_sig
	BEFORE UPDATE OR INSERT
	ON sditec_voirie.geo_voirie_communale
	FOR EACH ROW 
	EXECUTE PROCEDURE sditec_voirie.f_longtronc_sig();


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

/*Vue Chemins Ruraux*/

CREATE OR REPLACE VIEW sditec_voirie.v_geo_chemin_rural AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS statut_voie_libelle,
    a.appellation,
    a.p_origine,
    a.p_extremite,
	a.lieudit,
    a.longtronc_saisie,
	a.longtronc_sig,
	a.longueur_totale,
    a.largeur_emprise_moy,
    a.ancienne_voie,
    d.libelle AS existence_libelle,
    a.annee_classement,
    a.observations,
    a.url,
    a.origdata,
	a.date_creation,
	a.date_maj,
	a.statut_voie,
	a.existence_dernier_classement,
    a.the_geom
   FROM sditec_voirie.geo_voirie_communale a
     LEFT JOIN sditec_voirie.lst_statut_voie b ON a.statut_voie::text = b.code::text
     LEFT JOIN sditec_voirie.lst_existence d ON a.existence_dernier_classement::text = d.code::text
	WHERE a.statut_voie ='05';	 
	 

ALTER TABLE sditec_voirie.v_geo_chemin_rural
    OWNER TO sditecgrp;

/*Vue Voies Communales*/

CREATE OR REPLACE VIEW sditec_voirie.v_geo_voie_communale AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS statut_voie_libelle,
    c.libelle AS caractere_voie_libelle,
    a.appellation,
    a.p_origine,
    a.p_extremite,
	a.lieudit,	
    a.longtronc_saisie,
	a.longtronc_sig,
	a.longueur_totale,
    a.largeur_emprise_moy,
    a.ancienne_voie,
    d.libelle AS existence_libelle,	
    a.annee_classement,
    a.observations,
    a.url,
    a.origdata,
	a.date_creation,
	a.date_maj,
	a.statut_voie,
	a.caractere_voie,
	a.existence_dernier_classement,	
    a.the_geom
   FROM sditec_voirie.geo_voirie_communale a
     LEFT JOIN sditec_voirie.lst_statut_voie b ON a.statut_voie::text = b.code::text
     LEFT JOIN sditec_voirie.lst_caractere_voie c ON a.caractere_voie::text = c.code::text
     LEFT JOIN sditec_voirie.lst_existence d ON a.existence_dernier_classement::text = d.code::text
	WHERE a.statut_voie ='04';	 
	 

ALTER TABLE sditec_voirie.v_geo_voie_communale
    OWNER TO sditecgrp;


/*Vue Voies Communales - Rue*/

CREATE OR REPLACE VIEW sditec_voirie.v_geo_voie_communale_rue AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS statut_voie_libelle,
    c.libelle AS caractere_voie_libelle,
    a.appellation,
    a.p_origine,
    a.p_extremite,
	a.lieudit,	
    a.longtronc_saisie,
	a.longtronc_sig,
	a.longueur_totale,
    a.largeur_emprise_moy,
    a.ancienne_voie,
    d.libelle AS existence_libelle,	
    a.annee_classement,
    a.observations,
    a.url,
    a.origdata,
	a.date_creation,
	a.date_maj,
	a.statut_voie,
	a.caractere_voie,
	a.existence_dernier_classement,
    a.the_geom
   FROM sditec_voirie.geo_voirie_communale a
     LEFT JOIN sditec_voirie.lst_statut_voie b ON a.statut_voie::text = b.code::text
     LEFT JOIN sditec_voirie.lst_caractere_voie c ON a.caractere_voie::text = c.code::text
     LEFT JOIN sditec_voirie.lst_existence d ON a.existence_dernier_classement::text = d.code::text
	WHERE a.statut_voie ='04' AND a.caractere_voie ='1';	 
	 

ALTER TABLE sditec_voirie.v_geo_voie_communale_rue
    OWNER TO sditecgrp;


/*Vue Voies Communales - Chemin*/

CREATE OR REPLACE VIEW sditec_voirie.v_geo_voie_communale_chemin AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS statut_voie_libelle,
    c.libelle AS caractere_voie_libelle,
    a.appellation,
    a.p_origine,
    a.p_extremite,
	a.lieudit,	
    a.longtronc_saisie,
	a.longtronc_sig,
	a.longueur_totale,
    a.largeur_emprise_moy,
    a.ancienne_voie,
    d.libelle AS existence_libelle,	
    a.annee_classement,
    a.observations,
    a.url,
    a.origdata,
	a.date_creation,
	a.date_maj,
	a.statut_voie,
	a.caractere_voie,
	a.existence_dernier_classement,
    a.the_geom
   FROM sditec_voirie.geo_voirie_communale a
     LEFT JOIN sditec_voirie.lst_statut_voie b ON a.statut_voie::text = b.code::text
     LEFT JOIN sditec_voirie.lst_caractere_voie c ON a.caractere_voie::text = c.code::text
     LEFT JOIN sditec_voirie.lst_existence d ON a.existence_dernier_classement::text = d.code::text
	WHERE a.statut_voie ='04' AND a.caractere_voie ='2';	 
	 

ALTER TABLE sditec_voirie.v_geo_voie_communale_chemin
    OWNER TO sditecgrp;


/*Vue Voies Communales - Place*/

CREATE OR REPLACE VIEW sditec_voirie.v_geo_voie_communale_place AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS statut_voie_libelle,
    c.libelle AS caractere_voie_libelle,
    a.appellation,
    a.p_origine,
    a.p_extremite,
	a.lieudit,	
    a.longtronc_saisie,
	a.longtronc_sig,
	a.longueur_totale,
    a.largeur_emprise_moy,
    a.ancienne_voie,
    d.libelle AS existence_libelle,	
    a.annee_classement,
    a.observations,
    a.url,
    a.origdata,
	a.date_creation,
	a.date_maj,
	a.statut_voie,
	a.caractere_voie,
	a.existence_dernier_classement,
    a.the_geom
   FROM sditec_voirie.geo_voirie_communale a
     LEFT JOIN sditec_voirie.lst_statut_voie b ON a.statut_voie::text = b.code::text
     LEFT JOIN sditec_voirie.lst_caractere_voie c ON a.caractere_voie::text = c.code::text
     LEFT JOIN sditec_voirie.lst_existence d ON a.existence_dernier_classement::text = d.code::text
	WHERE a.statut_voie ='04' AND a.caractere_voie ='3';	 
	 

ALTER TABLE sditec_voirie.v_geo_voie_communale_place
    OWNER TO sditecgrp;




------------------------------------------------------
---/* TABLEAU DE CLASSEMENT DES VOIES COMMUNALES */---
-/* Vue d�finie en collabortion avec Pierre Sauze */-- 
------------------------------------------------------

/* CLASSEMENT TOUTES VC*/

CREATE OR REPLACE VIEW sditec_voirie.v_tb_classement_vc AS
 SELECT distinct v1.ident,
    v1.insee,
	v1.statut_voie_libelle,
	v1.caractere_voie_libelle,
    v1.appellation,
    v1.p_origine,
    v1.p_extremite,
	v1.lieudit,	
    v1.longueur_totale,
	v1.largeur_emprise_moy,
    v1.ancienne_voie,
	v1.existence_libelle,	
    v1.annee_classement,
    v1.observations,
	v1.url	
   FROM sditec_voirie.v_geo_voie_communale v1  
   WHERE v1.gid = (SELECT MAX(v2.gid) FROM sditec_voirie.v_geo_voie_communale v2 WHERE v2.insee=v1.insee AND v2.ident=v1.ident)
	ORDER BY v1.ident ASC;

ALTER TABLE sditec_voirie.v_tb_classement_vc
    OWNER TO sditecgrp;

/* CLASSEMENT VC RUE*/

CREATE OR REPLACE VIEW sditec_voirie.v_tb_classement_vc_rue AS
 SELECT distinct v1.ident,
    v1.insee,
	v1.statut_voie_libelle,
	v1.caractere_voie_libelle,
    v1.appellation,
    v1.p_origine,
    v1.p_extremite,
	v1.lieudit,	
    v1.longueur_totale,
	v1.largeur_emprise_moy,
    v1.ancienne_voie,
	v1.existence_libelle,	
    v1.annee_classement,
    v1.observations,
	v1.url	
   FROM sditec_voirie.v_geo_voie_communale_rue v1  
   WHERE v1.gid = (SELECT MAX(v2.gid) FROM sditec_voirie.v_geo_voie_communale_rue v2 WHERE v2.insee=v1.insee AND v2.ident=v1.ident)
	ORDER BY v1.ident ASC;

ALTER TABLE sditec_voirie.v_tb_classement_vc_rue
    OWNER TO sditecgrp;

/* CLASSEMENT VC CHEMIN*/

CREATE OR REPLACE VIEW sditec_voirie.v_tb_classement_vc_chemin AS
 SELECT distinct v1.ident,
    v1.insee,
	v1.statut_voie_libelle,
	v1.caractere_voie_libelle,
    v1.appellation,
    v1.p_origine,
    v1.p_extremite,
	v1.lieudit,	
    v1.longueur_totale,
	v1.largeur_emprise_moy,
    v1.ancienne_voie,
	v1.existence_libelle,	
    v1.annee_classement,
    v1.observations,
	v1.url	
   FROM sditec_voirie.v_geo_voie_communale_chemin v1  
   WHERE v1.gid = (SELECT MAX(v2.gid) FROM sditec_voirie.v_geo_voie_communale_chemin v2 WHERE v2.insee=v1.insee AND v2.ident=v1.ident)
	ORDER BY v1.ident ASC;

ALTER TABLE sditec_voirie.v_tb_classement_vc_chemin
    OWNER TO sditecgrp;

/* CLASSEMENT VC PLACE*/

CREATE OR REPLACE VIEW sditec_voirie.v_tb_classement_vc_place AS
 SELECT distinct v1.ident,
    v1.insee,
	v1.statut_voie_libelle,
	v1.caractere_voie_libelle,
    v1.appellation,
    v1.p_origine,
    v1.p_extremite,
	v1.lieudit,	
    v1.longueur_totale,
	v1.largeur_emprise_moy,
    v1.ancienne_voie,
	v1.existence_libelle,	
    v1.annee_classement,
    v1.observations,
	v1.url	
   FROM sditec_voirie.v_geo_voie_communale_place v1  
   WHERE v1.gid = (SELECT MAX(v2.gid) FROM sditec_voirie.v_geo_voie_communale_place v2 WHERE v2.insee=v1.insee AND v2.ident=v1.ident)
	ORDER BY v1.ident ASC;

ALTER TABLE sditec_voirie.v_tb_classement_vc_place
    OWNER TO sditecgrp;
