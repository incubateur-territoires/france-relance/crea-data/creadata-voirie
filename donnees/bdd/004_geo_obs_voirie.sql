-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Observation voirie                                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.geo_obs_voirie;

CREATE TABLE sditec_voirie.geo_obs_voirie
(
  gid serial NOT NULL,
  insee character varying(6),				/* N° insee de la commune */
  ident character varying(40), 				/* non utilisé */	
  observation varchar(255),  				/* Observations txt */
  origdata character varying(255),			/* Origine des données */  
  date_creation timestamp,					/* Date de création dans le SIG */
  date_maj timestamp,						/* Date de dernière modification dans le SIG*/	 
  the_geom geometry,
  CONSTRAINT pk_geo_obs_voirie PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sditec_voirie.geo_obs_voirie
  OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.geo_obs_voirie TO sditecgrp;

-- ################################################################## Commentaires ##################################################################

COMMENT ON COLUMN sditec_voirie.geo_obs_voirie.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_voirie.geo_obs_voirie.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN sditec_voirie.geo_obs_voirie.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_voirie.geo_noeud_filaire_voirie.observation IS '[ATD16] texte sur l''observation';
COMMENT ON COLUMN sditec_voirie.geo_obs_voirie.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN sditec_voirie.geo_obs_voirie.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Trigger(s)                                                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER sditec_voirie.t_before_i_date_creation ON sditec_voirie.geo_obs_voirie;

CREATE TRIGGER t_before_i_date_creation
	BEFORE INSERT 
	ON sditec_voirie.geo_obs_voirie
	FOR EACH ROW 
	EXECUTE PROCEDURE  sditec_voirie.f_date_creation();

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER sditec_voirie.t_before_u_date_maj ON sditec_voirie.geo_obs_voirie ;

CREATE TRIGGER t_before_u_date_maj
	BEFORE UPDATE 
	ON sditec_voirie.geo_obs_voirie 
	FOR EACH ROW 
	EXECUTE PROCEDURE  sditec_voirie.f_date_maj();