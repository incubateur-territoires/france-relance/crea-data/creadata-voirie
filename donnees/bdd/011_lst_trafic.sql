-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                          Table non géographique : Domaine de valeur du trafic                                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.lst_trafic;

CREATE TABLE sditec_voirie.lst_trafic 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code du trafic
    libelle varchar(254), --[ATD16] Libellé du trafic
    CONSTRAINT pk_lst_trafic PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.lst_trafic OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.lst_trafic TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.lst_trafic IS '[ATD16] Domaine de valeur de l''importance du trafic';

COMMENT ON COLUMN sditec_voirie.lst_trafic.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_voirie.lst_trafic.code IS '[ATD16] Code du trafic';
COMMENT ON COLUMN sditec_voirie.lst_trafic.libelle IS '[ATD16] Libellé du trafic';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_voirie.lst_trafic 
    (code, libelle)
VALUES
    ('01', 'Faible'),
    ('02', 'Moyen'),
    ('03', 'Important');

