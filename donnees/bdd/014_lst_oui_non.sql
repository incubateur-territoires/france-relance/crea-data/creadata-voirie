-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                             Table non géographique : Domaine de valeur Oui/Non                                             ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.lst_oui_non;

CREATE TABLE sditec_voirie.lst_oui_non 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la liste
    libelle varchar(254), --[ATD16] Libellé de la liste
    CONSTRAINT pk_lst_oui_non PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.lst_oui_non OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.lst_oui_non TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.lst_oui_non IS '[ATD16] Domaine de valeur Oui/Non';

COMMENT ON COLUMN sditec_voirie.lst_oui_non.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_voirie.lst_oui_non.code IS '[ATD16] Code de la liste';
COMMENT ON COLUMN sditec_voirie.lst_oui_non.libelle IS '[ATD16] Libellé de la liste';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_voirie.lst_oui_non 
    (code, libelle)
VALUES
    ('01', 'Oui'),
    ('02', 'Non');

