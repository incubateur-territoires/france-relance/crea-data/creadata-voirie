-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Cr�ation du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non g�ographique : Liste existnce de la voie (oui/non)                                               ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE sditec_voirie.lst_existence
(
  gid serial NOT NULL,
  libelle character varying,
  code character varying(2),
  CONSTRAINT lst_existence_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sditec_voirie.lst_existence
  OWNER TO sditecgrp;
GRANT ALL ON TABLE sditec_voirie.lst_existence TO sditecgrp;


/* Insertion des donn�es dans lst_existence */

INSERT INTO sditec_voirie.lst_existence (code, libelle)
	VALUES
	('01', 'Oui'), 
	('02', 'Non');
