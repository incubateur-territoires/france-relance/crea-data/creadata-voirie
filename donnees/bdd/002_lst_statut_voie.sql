-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non géographique : Liste du statut de la voie                                                        ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE sditec_voirie.lst_statut_voie
(
  gid serial NOT NULL,
  code character varying(10),
  libelle character varying(256),
  CONSTRAINT lst_statut_voie_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sditec_voirie.lst_statut_voie
  OWNER TO xmapadmgrp;

GRANT ALL ON TABLE sditec_voirie.lst_statut_voie TO xmapadmgrp;
GRANT ALL ON TABLE sditec_voirie.lst_statut_voie TO xmapgrp;


/* Insertion des données dans lst_statut_voie */

INSERT INTO sditec_voirie.lst_statut_voie (code, libelle)
	VALUES
	('00', 'Indéterminée'), 
	/*('01', 'Nationale'),*/
	/*('02', 'Départementale'),*/
	('03', 'Intercommunale'), 
	('04', 'Communale'),
	('05', 'Rurale'),
	/*('06', 'Privée')*/;
