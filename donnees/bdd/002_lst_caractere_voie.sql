-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Cr�ation du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                 Table non g�ographique : Liste du carct�re de la voie                                                      ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

CREATE TABLE sditec_voirie.lst_caractere_voie
(
  gid serial NOT NULL,
  libelle character varying,
  code character varying(10),
  CONSTRAINT lst_caractere_voie_pkey PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE sditec_voirie.lst_caractere_voie
  OWNER TO xmapadmgrp;
GRANT ALL ON TABLE sditec_voirie.lst_caractere_voie TO xmapadmgrp;
GRANT ALL ON TABLE sditec_voirie.lst_caractere_voie TO xmapgrp;


/* Insertion des donn�es dans lst_caractere_voie */

INSERT INTO sditec_voirie.lst_caractere_voie (code, libelle)
	VALUES
	('0', 'Ind�termin�e'), 
	('1', 'Rue'),
	('2', 'Chemin'),
	('3', 'Place');
