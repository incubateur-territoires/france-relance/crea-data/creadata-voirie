-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/03 : SL / Création du fichier sur Git
-- 2022/05/13 : SL / Correction du trigger t_before_iu_maj_longtronc_sig. Il s'applique désormais à la fonction f_longtronc_sig_alt()
-- 2022/05/16 : SL / Ajout des champs [CCLTD]

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                        Table géographique : Programme d'entretien de la voirie (cdc)                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.geo_programme_entretien_cdc;

CREATE TABLE sditec_voirie.geo_programme_entretien_cdc
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations
    insee varchar(6), --[ATD16] Code INSEE de la commune
    ident varchar(50), --[SIRAP] Identifiant SIRAP / Nom du tronçon, de la voie, de la voirie ou du programme au choix du créateur
    numero_voie varchar(20), --[ATD16] Numéro de la voie
    nom_voie varchar(255), --[ATD16] Nom de la voie
    longtronc_sig real, --[ATD16] Longueur du tronçon (semi-automatique)
    trafic_leger varchar(2), --[CCLTD] Appréciation du trafic léger sur la voie
    trafic_lourd varchar(2), --[CCLTD] Appréciation du trafic lourd sur la voie
    nature_ss_sol varchar(2), --[CCLTD] Nature du sous-sol
    secteur_boise varchar(2), --[CCLTD] Présence de secteur boisé
    etat_route varchar(2), --[CCLTD] État général actuel de la voie
    voie_transit varchar(2), --[CCLTD] Voie de transit (liaison entre voie importante, desserte village)
    nature_revetement varchar(255), --[CCLTD] Nature du revêtement actuel
    date_creation date, --[ATD16] Date de création de l'objet (automatique)
    date_maj date, --[ATD16] Date de dernière mise à jour de l'objet (automatique)
    origdata varchar(254), --[ATD16] Provenance de la donnée
    the_geom geometry, --[ATD16] Champ contenant le géométrie
    CONSTRAINT pk_geo_programme_entretien_cdc PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.geo_programme_entretien_cdc OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.geo_programme_entretien_cdc TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.geo_programme_entretien_cdc IS 
    '[ATD16] Table contenant la géométrie des tronçons de voiries faisant partie d''un programme d''entretien par la cdc. Actuellement la ccltd y recense les tronçons prioritaires. Table destinée à la construction.';

COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.ident IS 
    '[SIRAP] Identifiant SIRAP / Nom du tronçon, de la voie, de la voirie ou du programme au choix du créateur';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.numero_voie IS '[ATD16] Numéro de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.nom_voie IS '[ATD16] Nom de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.longtronc_sig IS '[ATD16] Longueur de l''objet (semi-automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.trafic_leger IS '[CCLTD] Appréciation du trafic léger sur la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.trafic_lourd IS '[CCLTD] Appréciation du trafic lourd sur la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.nature_ss_sol IS '[CCLTD] Nature du sous-sol';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.secteur_boise IS '[CCLTD] Présence de secteur boisé';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.etat_route IS '[CCLTD] État général actuel de la voie';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.voie_transit IS '[CCLTD] Voie de transit (liaison entre voie importante, desserte village)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.nature_revetement IS '[CCLTD] Nature du revêtement actuel';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.origdata IS '[ATD16] Provenance de la donnée';
COMMENT ON COLUMN sditec_voirie.geo_programme_entretien_cdc.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Fonction(s) trigger(s)                                                           ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_i_init_date_creation ON sditec_voirie.geo_programme_entretien_cdc;

CREATE TRIGGER t_before_i_init_date_creation
    BEFORE INSERT
    ON sditec_voirie.geo_programme_entretien_cdc
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_voirie.f_date_creation();
    
COMMENT ON TRIGGER t_before_i_init_date_creation ON sditec_voirie.geo_programme_entretien_cdc IS 
    'Trigger déclenchant la fonction permettant l''initialisation du champ date_creation';
    

-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_u_date_maj ON sditec_voirie.geo_programme_entretien_cdc;

CREATE TRIGGER t_before_u_date_maj
    BEFORE UPDATE
    ON sditec_voirie.geo_programme_entretien_cdc
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_voirie.f_date_maj();
    
COMMENT ON TRIGGER t_before_u_date_maj ON sditec_voirie.geo_programme_entretien_cdc IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';


-- ##################################################################################################################################################
-- ###                                                     Mise à jour du champ longtronc_sig                                                     ###
-- ##################################################################################################################################################

-- DROP TRIGGER t_before_iu_maj_longtronc_sig ON sditec_voirie.geo_programme_entretien_cdc;

CREATE TRIGGER t_before_iu_maj_longtronc_sig
    BEFORE INSERT OR UPDATE
    ON sditec_voirie.geo_programme_entretien_cdc
    FOR EACH ROW
    EXECUTE PROCEDURE sditec_voirie.f_longtronc_sig_alt();
    
COMMENT ON TRIGGER t_before_iu_maj_longtronc_sig ON sditec_voirie.geo_programme_entretien_cdc IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ longtronc_sig lorsqu''il est NULL';

