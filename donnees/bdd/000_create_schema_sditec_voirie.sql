-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021-11-24 : RL / Migration du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                          SCHEMA : Gestion de la voirie                                                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################




-- DROP SCHEMA sditec_voirie;

CREATE SCHEMA sditec_voirie
  AUTHORIZATION sditecgrp;
  
COMMENT ON SCHEMA sditec_voirie IS 'Gestion voirie'  

GRANT ALL ON SCHEMA sditec_voirie TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA sditec_voirie
    GRANT INSERT, SELECT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER ON TABLES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA sditec_voirie
    GRANT SELECT, UPDATE, USAGE ON SEQUENCES
    TO sditecgrp;

ALTER DEFAULT PRIVILEGES IN SCHEMA sditec_voirie
    GRANT EXECUTE ON FUNCTIONS
    TO sditecgrp;

