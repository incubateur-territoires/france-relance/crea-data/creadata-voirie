-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                     Table non géographique : Liste des natures noeud (intersections)                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


CREATE TABLE sditec_voirie.lst_nature_noeud
(
    id serial NOT NULL,
    libelle character varying,
    abreviation character varying,	
    "order" integer,
    CONSTRAINT sditec_voirie_lst_nature_noeud_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE sditec_voirie.lst_nature_noeud
    OWNER to sditecgrp;
GRANT ALL ON TABLE sditec_voirie.lst_nature_noeud TO sditecgrp;

	
-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_voirie.lst_nature_noeud (libelle, abreviation, "order")
	VALUES
	('Intersection','VOIRIE', 0),
	('Changement de domanialité','DOMANIALITE', 1),
	('Changement de commune','COMMUNE', 2),	
	('Passage','PASSAGE', 3),	
	('Changement de circulation','CIRCULATION', 4);	
