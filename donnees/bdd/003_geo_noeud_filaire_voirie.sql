-- ################################################################# SUIVI CODE SQL #################################################################

-- 2021/11/24 : RL / Création du fichier sur Git


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                               Table géographique : Noeud filaire voirie (intersections)                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.geo_noeud_filaire_voirie;

CREATE TABLE sditec_voirie.geo_noeud_filaire_voirie
(
    gid serial NOT NULL,
    insee character varying,				/* N° insee de la commune */
    ident character varying,				/* Libellé intersection */
    nature character varying,				/* Nature de l'intersection */
	date_creation timestamp, 				/* Date de création dans le SIG */
    date_maj timestamp, 					/* Date de dernière modification dans le SIG*/
	the_geom geometry,
    CONSTRAINT sditec_voirie_geo_noeud_filaire_voirie_pk PRIMARY KEY (gid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE sditec_voirie.geo_noeud_filaire_voirie
    OWNER to sditecgrp;
GRANT ALL ON TABLE sditec_voirie.geo_noeud_filaire_voirie TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON COLUMN sditec_voirie.geo_noeud_filaire_voirie.gid IS '[PK][ATD16] Identifiant unique généré automatiquement au fil des intégrations';
COMMENT ON COLUMN sditec_voirie.geo_noeud_filaire_voirie.ident IS '[SIRAP] Identificaion de l''objet';
COMMENT ON COLUMN sditec_voirie.geo_noeud_filaire_voirie.insee IS '[ATD16] Code INSEE de la commune';
COMMENT ON COLUMN sditec_voirie.geo_noeud_filaire_voirie.nature IS '[FK][lst_nature_noeud] Nature de l''intersection';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.date_creation IS '[ATD16] Date de création de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.date_maj IS '[ATD16] Date de dernière mise à jour de l''objet (automatique)';
COMMENT ON COLUMN atd16_espaces_verts.geo_zone_boisee_com.the_geom IS '[ATD16] Champ contenant le géométrie';


-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                           Trigger(s)                                                                       ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP TRIGGER sditec_voirie.t_before_i_date_creation ON sditec_voirie.geo_noeud_filaire_voirie;

CREATE TRIGGER t_before_i_date_creation 
	BEFORE INSERT 
	ON sditec_voirie.geo_noeud_filaire_voirie
	FOR EACH ROW 
	EXECUTE PROCEDURE sditec_voirie.f_date_creation();

COMMENT ON TRIGGER t_before_i_date_creation ON sditec_voirie.geo_noeud_filaire_voirie IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_creation';


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP TRIGGER sditec_voirie.t_before_u_date_maj ON sditec_voirie.geo_noeud_filaire_voirie;

CREATE TRIGGER t_before_u_date_maj 
	BEFORE UPDATE
	ON sditec_voirie.geo_noeud_filaire_voirie
	FOR EACH ROW
	EXECUTE PROCEDURE  sditec_voirie.f_date_maj();
	
COMMENT ON TRIGGER t_before_u_date_maj ON asditec_voirie.geo_noeud_filaire_voirie IS 
    'Trigger déclenchant la fonction permettant la mise à jour du champ date_maj';



-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                                   Vue(s)                                                                   ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

/* VUE_NOEUD - Vue des intersections - point */

CREATE OR REPLACE VIEW sditec_voirie.v_geo_noeud_filaire_voirie AS
 SELECT a.gid,
    a.insee,
    a.ident,
    b.libelle AS nature_libelle,
   	a.date_creation,
	a.date_maj,
    a.the_geom
   FROM sditec_voirie.geo_noeud_filaire_voirie a
     LEFT JOIN sditec_voirie.lst_nature_noeud b ON a.nature::text = b.abreviation::text;	 
	 

ALTER TABLE sditec_voirie.v_geo_noeud_filaire_voirie
    OWNER TO sditecgrp;

