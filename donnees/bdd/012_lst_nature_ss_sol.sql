-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                    Table non géographique : Domaine de valeur de la nature du sous-sol                                     ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.lst_nature_ss_sol;

CREATE TABLE sditec_voirie.lst_nature_ss_sol 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de la nature du sous-sol
    libelle varchar(254), --[ATD16] Libellé de la nature du sous-sol
    CONSTRAINT pk_lst_nature_ss_sol PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.lst_nature_ss_sol OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.lst_nature_ss_sol TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.lst_nature_ss_sol IS '[ATD16] Domaine de valeur de la nature du sous-sol';

COMMENT ON COLUMN sditec_voirie.lst_nature_ss_sol.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_voirie.lst_nature_ss_sol.code IS '[ATD16] Code de la nature du sous-sol';
COMMENT ON COLUMN sditec_voirie.lst_nature_ss_sol.libelle IS '[ATD16] Libellé de la nature du sous-sol';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_voirie.lst_nature_ss_sol 
    (code, libelle)
VALUES
    ('01', 'Mauvais'),
    ('02', 'Moyen'),
    ('03', 'Bon');

