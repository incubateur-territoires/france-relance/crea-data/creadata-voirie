-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/13 : SL / Création du fichier sur Git
--                 . Ajout de la fonction f_date_creation()
--                 . Ajout de la fonction f_date_maj()
--                 . Ajout de la fonction f_date_fin_arrete()
--                 . Ajout de la fonction f_longtronc_sig()
--                 . Ajout de la fonction f_longtronc_sig_alt()

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                                     Fonction(s) trigger(s) générique(s)                                                    ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################


-- ##################################################################################################################################################
-- ###                                                   Initialisation du champ date_creation                                                    ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS sditec_voirie.f_date_creation();

CREATE OR REPLACE FUNCTION sditec_voirie.f_date_creation()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_creation = now();
    RETURN NEW;   
END;

$BODY$;

ALTER FUNCTION sditec_voirie.f_date_creation() OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION sditec_voirie.f_date_creation() TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_maj                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS sditec_voirie.f_date_maj();

CREATE OR REPLACE FUNCTION sditec_voirie.f_date_maj()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_maj = now();
    RETURN NEW;   
END;

$BODY$;

ALTER FUNCTION sditec_voirie.f_date_maj() OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION sditec_voirie.f_date_maj() TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                       Mise à jour du champ date_fin                                                        ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS sditec_voirie.f_date_fin_arrete();

CREATE OR REPLACE FUNCTION sditec_voirie.f_date_fin_arrete()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

BEGIN
    NEW.date_fin = new.date_debut + new.nb_jours * INTERVAL '1' DAY;
    RETURN NEW;   
END;

$BODY$;

ALTER FUNCTION sditec_voirie.f_date_fin_arrete()
    OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION sditec_voirie.f_date_fin_arrete() TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                     Mise à jour du champ longtronc_sig                                                     ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS sditec_voirie.f_longtronc_sig();

CREATE OR REPLACE FUNCTION sditec_voirie.f_longtronc_sig()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
    DECLARE
    
    BEGIN
        NEW.longtronc_sig := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); 
/*Obligation de transformer la projection car en 3857, la distance est augmentée de 50%*/
        RETURN NEW;
    END;

$BODY$;

ALTER FUNCTION sditec_voirie.f_longtronc_sig() OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION sditec_voirie.f_longtronc_sig() TO sditecgrp;


-- ##################################################################################################################################################
-- ###                                                     Mise à jour du champ longtronc_sig                                                     ###
-- ##################################################################################################################################################

-- DROP FUNCTION IF EXISTS sditec_voirie.f_longtronc_sig_alt();

CREATE OR REPLACE FUNCTION sditec_voirie.f_longtronc_sig_alt()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$

DECLARE
    
BEGIN
    IF NEW.longtronc_sig IS NULL THEN -- Si le champ a une valeur NULL alors
        NEW.longtronc_sig := ST_Length(ST_Transform(ST_SetSRID(NEW.the_geom,3857),2154)); 
/*Obligation de transformer la projection car en 3857, la distance est augmentée de 50%*/
    END IF;
    RETURN NEW;
END;

$BODY$;

ALTER FUNCTION sditec_voirie.f_longtronc_sig_alt() OWNER TO sditecgrp;

GRANT EXECUTE ON FUNCTION sditec_voirie.f_longtronc_sig_alt() TO sditecgrp;

COMMENT ON FUNCTION sditec_voirie.f_longtronc_sig_alt() IS '[ATD16] Mise à jour du champ longtronc_sig lorsque sa valeur est NULL';

