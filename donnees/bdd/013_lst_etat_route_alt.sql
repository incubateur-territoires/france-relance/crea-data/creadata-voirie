-- ################################################################# SUIVI CODE SQL #################################################################

-- 2022/05/13 : SL / Création du fichier sur Git

-- ##################################################################################################################################################
-- ###                                                                                                                                            ###
-- ###                                  Table non géographique : Domaine de valeur de l'état de la route (CCLTD)                                  ###
-- ###                                                                                                                                            ###
-- ##################################################################################################################################################

-- DROP TABLE sditec_voirie.lst_etat_route_alt;

CREATE TABLE sditec_voirie.lst_etat_route_alt 
(
    gid serial NOT NULL, --[PK][ATD16] Identifiant unique généré automatiquement
    code varchar(2), --[ATD16] Code de l'état de la route
    libelle varchar(254), --[ATD16] Libellé de l'état de la route
    CONSTRAINT pk_lst_etat_route_alt PRIMARY KEY (gid)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE sditec_voirie.lst_etat_route_alt OWNER TO sditecgrp;

GRANT ALL ON TABLE sditec_voirie.lst_etat_route_alt TO sditecgrp;


-- ################################################################## Commentaires ##################################################################

COMMENT ON TABLE sditec_voirie.lst_etat_route_alt IS '[ATD16] Domaine de valeur de l''état de la route (à partir du fichier XLS de Betty MOREAU)';

COMMENT ON COLUMN sditec_voirie.lst_etat_route_alt.gid IS '[ATD16] Identifiant unique généré automatiquement';
COMMENT ON COLUMN sditec_voirie.lst_etat_route_alt.code IS '[ATD16] Code de l''état de la route';
COMMENT ON COLUMN sditec_voirie.lst_etat_route_alt.libelle IS '[ATD16] Libellé de l''état de la route';


-- ############################################################ Ajout des enregistrements ###########################################################

INSERT INTO sditec_voirie.lst_etat_route_alt 
    (code, libelle)
VALUES
    ('01', 'Mauvais'),
    ('02', 'Moyen'),
    ('03', 'Bon');

