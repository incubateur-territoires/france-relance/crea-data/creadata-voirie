<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <NamedLayer>
    <Name>v_geo_voie_communale</Name>
    <UserStyle>
      <Name>v_geo_voie_communale</Name>
      <FeatureTypeStyle>
        <Rule>
          <TextSymbolizer>
			<Label>
				<ogc:PropertyName>ident</ogc:PropertyName>
			</Label>
			 <LabelPlacement>
			   <LinePlacement>
					<PerpendicularOffset>
						15
					</PerpendicularOffset>
				</LinePlacement>
			 </LabelPlacement>
            <Halo>
           		<Radius><ogc:Literal>2</ogc:Literal></Radius>
           		<Fill>
             		<CssParameter name="fill">#FFFFFF</CssParameter>
           		</Fill>
         	</Halo>
            <Fill>
              <CssParameter name="fill">#316da4</CssParameter>
            </Fill>
			<VendorOption name="followLine">true</VendorOption>
		   </TextSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
