<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des toponymes des programmes d'entretien de la CC LTD
    appliquée à la couche voirie:geo_programme_entretien_cdc_tpn (Geoserver) ; Voirie\Programme d'entretien\Numéro des voies
-->

<StyledLayerDescriptor version="1.0.0" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>programme_entretien</Name>
        <UserStyle>
            <Name>programme_entretien</Name>
            <FeatureTypeStyle>
                <Rule>
                    <TextSymbolizer>
                        <Label>
                            <ogc:PropertyName>numero_voie</ogc:PropertyName>
                        </Label>
                        <LabelPlacement>
                            <LinePlacement>
                                <PerpendicularOffset>15</PerpendicularOffset>
                            </LinePlacement>
                        </LabelPlacement>
                        <Halo>
                            <Radius><ogc:Literal>2</ogc:Literal></Radius>
                            <Fill>
                                <CssParameter name="fill">#FFFFFF</CssParameter>
                            </Fill>
                        </Halo>
                        <Fill>
                            <CssParameter name="fill">#FF0000</CssParameter>
                        </Fill>
                        <VendorOption name="followLine">true</VendorOption>
                    </TextSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
