<?xml version="1.0" encoding="UTF-8"?>

<!--
Représentation thématique des programmes d'entretien initiaux de la CC LTD
    appliquée à la couche voirie:geo_programme_entretien_save_tmp_cdc (Geoserver) ; Voirie\Programme d'entretien\Programme d'entretien initial ccltd (mai 2022)
-->

<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.1.0/StyledLayerDescriptor.xsd" xmlns:se="http://www.opengis.net/se">
    <NamedLayer>
        <se:Name>programme_entretien_initial</se:Name>
        <UserStyle>
            <se:Name>programme_entretien_initial</se:Name>
            <se:FeatureTypeStyle>
                <se:Rule>
                    <se:Name>programme_entretien_initial</se:Name>
                    <se:Description>
                        <se:Title>Programme d'entretien initial</se:Title>
                    </se:Description>
                    <se:LineSymbolizer>
                        <se:Stroke>
                            <se:SvgParameter name="stroke">#FFFFFF</se:SvgParameter>
                            <se:SvgParameter name="stroke-width">12</se:SvgParameter>
                            <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                            <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                  	<se:LineSymbolizer>
						<se:Stroke>
                          <se:SvgParameter name="stroke">#FFFF00</se:SvgParameter>
                          <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                          <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                          <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                    <se:LineSymbolizer>
                        <se:Stroke>
                          <se:SvgParameter name="stroke">#FF0000</se:SvgParameter>
                          <se:SvgParameter name="stroke-width">3</se:SvgParameter>
                          <se:SvgParameter name="stroke-linejoin">bevel</se:SvgParameter>
                          <se:SvgParameter name="stroke-linecap">square</se:SvgParameter>
                          <se:SvgParameter name="stroke-dasharray">4 8</se:SvgParameter>
                        </se:Stroke>
                    </se:LineSymbolizer>
                </se:Rule>
            </se:FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>
