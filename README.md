![](ressources/img/logo_geo16crea.png)
![](ressources/img/route2.jpg)

# Recensement de la voirie communale

Ce dépot recense l'ensemble des fichiers permettant la mise en place d'une base de données géographique permettant le recensement de la voirie communale.

Ce modèle est mis en oeuvre par l'ATD16 dans le cadre d'une utilisation sur le logiciel X'MAP développé par la société SIRAP. Il s'implémente dans un serveur de base de base de données PostgreSQL/PostGIS. 
Néanmoins il peut être adapté pour un usage sur un autre outil.

Le modèle est susceptible d'évoluer en fonction des besoins des collectivités.

# Principe de fonctionnement

Des tables géographiques permettent d'**inventorier** :
- voirie communale : élément linéaire permettant de localiser un tronçon de la voirie communale (voie communale ou chemin rural ...)
- noeud viaire : élément ponctuel permettant de localiser les extremités d'un tronçon (intersections, fin de voie, limite communale, etc...) 
- remarque : élément ponctuel permettant de localiser une remarque sur la voirie pour un traitement ultérieur. 


# Génèse : quelques dates

* septembre 2019 : demandes de la part des communes de pouvoir localiser leur voirie.
* mai 2020-septembre 2021 : développement d'un modèle de données permettant de recenser les voies communales et chemins ruraux.
* décembre 2021 : mise en production du modèle dans X'MAP.
* juin 2022 : ajout d'une table pour recenser les voies prioritaires à entretenir (groupement voirie CC LTD)

# Ressources du projet

* MCD [[pdf](ressources/mcd_voirie_communale.drawio.pdf)] - à faire  
* Schéma applicatif [[pdf](ressources/schema_applicatif_voirie_communale.drawio.pdf)] - à faire  

/!\ le shéma de bdd "sditec_voirie" est utilisé par l'ATD16 pour répondre aux exigences de son schéma applicatif. La modification de cette valeur nécessitera d'intervenir sur l'ensemble des fichiers de code. 

- Script agrégé de l'ensemble de la structure [[sql](donnees/bdd/aggregat_sditec_voirie.sql)]
- Schéma **sditec_voirie** [[sql](donnees/bdd/000_create_schema_sditec_voirie.sql)]
   * Fonctions-triggers génériques [[sql](donnees/bdd/001_create_function_trigger_generique.sql)]
    

## Voirie communale

   * Tables de listes déroulantes :
      * Table **lst_caractere_voie** [[sql](donnees/bdd/002_lst_caractere_voie.sql)]  
      * Table **lst_existence** [[sql](donnees/bdd/002_lst_existence.sql)] 
      * Table **lst_statut_voie** [[sql](donnees/bdd/002_lst_statut_voie.sql)]
   * Table géographique :
      * Table **geo_voirie_communale** [[sql](donnees/bdd/002_geo_voirie_communale.sql)] - [[sld](donnees/representation_graphique/sld/geo_voie_communale.sld)] - [[json](donnees/representation_graphique/json/geo_voirie_communale.json)]

## Noeud

   * Tables de listes déroulantes :
      * Table **lst_nature_noeud** [[sql](donnees/bdd/003_lst_nature_noeud.sql)]  
   * Table géographique :
      * Table **geo_noeud_filaire_voirie** [[sql](donnees/bdd/003_geo_noeud_filaire_voirie.sql)] - [[sld](donnees/representation_graphique/sld/geo_noeud_filaire_voirie.sld)] - [[json](donnees/representation_graphique/json/geo_noeud_filaire_voirie.json)]

## Observations

   * Tables géographiques :
      * Table **geo_obs_voirie** [[sql](donnees/bdd/004_geo_obs_voirie.sql)] - [[sld](donnees/representation_graphique/sld/geo_obs_voirie.sld)] - [[json](donnees/representation_graphique/json/.json)]  

## Programme d'entretien (Spéc CC LTD)

   * Tables de listes déroulantes :
      * Table **lst_trafic** [[sql](donnees/bdd/011_lst_trafic.sql)]  
      * Table **lst_nature_ss_sol** [[sql](donnees/bdd/012_lst_nature_ss_sol.sql)] 
      * Table **lst_etat_route_alt** [[sql](donnees/bdd/013_lst_etat_route_alt.sql)]
      * Table **lst_oui_non** [[sql](donnees/bdd/014_lst_oui_non.sql)]         
   * Table géographique :
      * Table **geo_programme_entretien_cdc** [[sql](donnees/bdd/213_geo_programme_entretien_cdc.sql)] - [[sld](donnees/representation_graphique/sld/voirie_programme_entretien_initial.sld)] - [[json](donnees/representation_graphique/json/lin_programme_entretien_voirie.json)]


# Documentation(s) 

* Notice d'utilisation dans X'MAP [[pdf](doc/G%C3%A9o16Cr%C3%A9a%20-%20Voirie%20communale%20et%20tableau%20de%20classement.pdf)]

